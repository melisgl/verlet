(in-package #:verlet)

(defun project (v p n)
  (=> v
    (v3:- % p)
    (v3:- % (v3:*s n (v3:dot % n)))
    (v3:+ % p)))

(defun orthogonalize (v p n)
  (=> v
    (v3:- % p)
    (v3:*s n (v3:dot % n))
    (v3:+ % p)))

(defun mirror (v p n)
  (=> v
    (v3:- % p)
    (v3:*s n (v3:dot % n))
    (v3:- v (v3:*s % 2.0))))

(defun towards (v target length)
  (=> v
    (v3:- target %)
    (v3:*s (v3:normalize %)
           length)
    (v3:+ v %)))
