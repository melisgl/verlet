(in-package #:verlet)

(defun union* (&rest sets)
  (reduce #'union
          sets
          :initial-value (set)))
