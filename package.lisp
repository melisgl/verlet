;;;; package.lisp

(mgl-pax:define-package #:verlet
    (:use #:cl #:metabang-bind #:chain #:rtg-math #:mgl-pax)
  (:shadowing-import-from
   #:fset
   #:set #:with #:less #:image #:remove-if #:union #:reduce #:convert
   #:do-set))

(in-package #:verlet)

(setf *document-max-numbering-level* 4
      *document-max-table-of-contents-level* 4)

(defsection @verlet (:title "Verlet physics library")
  (verlet asdf/system:system)
  (@interface section))

(defsection @interface (:title "Interface")
  (update! generic-function)
  (@force section)
  (@physics-system section)
  (@particle section)
  (@spring section)
  (@modificator section))
