<a id='x-28VERLET-3A-40VERLET-20MGL-PAX-3ASECTION-29'></a>

# Verlet physics library

## Table of Contents

- [1 VERLET ASDF System Details][376d]
- [2 Interface][6f25]
    - [2.1 Force][7130]
    - [2.2 Physics System][bb37]
    - [2.3 Particles][07c1]
    - [2.4 Springs][b17f]
        - [2.4.1 Linear Springs][6bfc]
        - [2.4.2 Area Springs][dda4]
        - [2.4.3 Volume Springs][6c05]
    - [2.5 Modificators][eb83]
        - [2.5.1 Damper][881d]
        - [2.5.2 Gravity][fd5b]
            - [2.5.2.1 Global Gravity][b8f0]
            - [2.5.2.2 Mutual Gravity][fbfc]
            - [2.5.2.3 Field Gravity][e2ab]
        - [2.5.3 Constraints][6115]
            - [2.5.3.1 Plane Constraints][0ebf]
            - [2.5.3.2 Sphere Constraints][7a53]
            - [2.5.3.3 Triangle Constraints][1a80]
        - [2.5.4 Friction][995d]
            - [2.5.4.1 Global Friction][7ba3]
            - [2.5.4.2 Field Friction][1bb9]
            - [2.5.4.3 Constraint Friction][1e57]

###### \[in package VERLET\]
<a id='x-28-22verlet-22-20ASDF-2FSYSTEM-3ASYSTEM-29'></a>

## 1 VERLET ASDF System Details

- Version: 0.0.8
- Description: Verlet is a simple physics engine based on verlet
  integration. It supports particles with position and direction, springs
  between particles, global gravity as well as gravity between particles, and
  spacial constraints.
- Licence: BSD-3
- Author: Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>


<a id='x-28VERLET-3A-40INTERFACE-20MGL-PAX-3ASECTION-29'></a>

## 2 Interface

<a id='x-28VERLET-3AUPDATE-21-20GENERIC-FUNCTION-29'></a>

- [generic-function] **UPDATE!** *OBJECT &OPTIONAL DELTA*

<a id='x-28VERLET-3A-40FORCE-20MGL-PAX-3ASECTION-29'></a>

### 2.1 Force

<a id='x-28VERLET-3ADEFORCE-20-28MGL-PAX-3AMACRO-29-29'></a>

- [macro] **DEFORCE** *NAME MODE (&REST LAMBDA-LIST) &BODY BODY*

    Define a force so it is usable in the [`BUILD-SYSTEM`][4857] macro.
    
    `MODE` is required to be one of `:FORCE`, `:VELOCITY-DEPENDENT-FORCE`, `:CONSTRAINT`,
    and `:COLLISION`. Reference the [`STEP!`][c236] function for more information on what these
    mean.
    
    The `LAMBDA-LIST` is tha lambda list used by the local macro in the [`BUILD-SYSTEM`][4857]
    macro call, `BODY` is it's body. The resulting code should return the object to
    add to the system under construction. `PARTICLES` is an anaphoric variable in the
    `BODY`, containing a list of the particles defined by `PARTICLES` in the
    [`BUILD-SYSTEM`][4857] macro, in the order they are defined in.

<a id='x-28VERLET-3AFORCE-20-28MGL-PAX-3ALOCATIVE-29-29'></a>

- [locative] **FORCE**

<a id='x-28VERLET-3A-40PHYSICS-SYSTEM-20MGL-PAX-3ASECTION-29'></a>

### 2.2 Physics System

<a id='x-28VERLET-3ASYSTEM-20CLASS-29'></a>

- [class] **SYSTEM**

<a id='x-28VERLET-3A-5BSYSTEM-5D-20FUNCTION-29'></a>

- [function] **\[SYSTEM\]** 

<a id='x-28VERLET-3ABUILD-SYSTEM-20-28MGL-PAX-3AMACRO-29-29'></a>

- [macro] **BUILD-SYSTEM** *(&REST PARTICLES) &BODY FORCES*

    Build up a standard physics [`SYSTEM`][a429] using defined forces.
    
    `PARTICLES` is a list with items of the form
    `((X Y Z) &KEY (MASS 1.0) PREVIOUS-POSITION VELOCITY)`.
    The list at the beginning describes the position of the particle, for the
    meaning of the keyword arguments reference the [`[PARTICLE]`][27d2] function.
    
    `FORCES` is a list of force descriptors, as defined by the [`DEFORCE`][55de] macro. The
    respective forces are going to be added to the final system.

<a id='x-28VERLET-3APARTICLES-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASYSTEM-29-29'></a>

- [accessor] **PARTICLES\<-** *SYSTEM* *(= (SET))*

<a id='x-28VERLET-3AFORCES-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASYSTEM-29-29'></a>

- [accessor] **FORCES\<-** *SYSTEM* *(= (SET))*

<a id='x-28VERLET-3AVELOCITY-DEPENDENT-FORCES-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASYSTEM-29-29'></a>

- [accessor] **VELOCITY-DEPENDENT-FORCES\<-** *SYSTEM* *(= (SET))*

<a id='x-28VERLET-3ACOLLISIONS-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASYSTEM-29-29'></a>

- [accessor] **COLLISIONS\<-** *SYSTEM* *(= (SET))*

<a id='x-28VERLET-3ACONSTRAINTS-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASYSTEM-29-29'></a>

- [accessor] **CONSTRAINTS\<-** *SYSTEM* *(= (SET))*

<a id='x-28VERLET-3ASTEP-21-20FUNCTION-29'></a>

- [function] **STEP!** *SYSTEM DELTA &KEY (RESOLVE-STEPS 1)*

    Updates a physical `SYSTEM` by timestep `DELTA`.
    
    1. Apply all forces not dependent on the velocity of particles (`:FORCE` in the
       [`DEFORCE`][55de] macro)
    
    2. Calculate forces depending on velocities of particles, using the memoized
       velocities so far (`:VELOCITY-DEPENDENT-FORCE` in the [`DEFORCE`][55de] macro)
    
    3. Repeat `RESOLVE-STEPS` times:
    
    4. Resolve all static constraints (`:CONSTRAINT` in the [`DEFORCE`][55de] macro)
    
    5. Resolve collisions (`:COLLISION` in the [`DEFORCE`][55de] macro)
    
    Please note after [`STEP!`][c236] returns, no collisions should be happening, but static
    constraints might be violated.

<a id='x-28VERLET-3A-40PARTICLE-20MGL-PAX-3ASECTION-29'></a>

### 2.3 Particles

<a id='x-28VERLET-3APARTICLE-20CLASS-29'></a>

- [class] **PARTICLE**

<a id='x-28VERLET-3A-5BPARTICLE-5D-20FUNCTION-29'></a>

- [function] **\[PARTICLE\]** *POSITION &KEY (MASS 1.0) PREVIOUS-POSITION VELOCITY*

    Returns an instance of class [`PARTICLE`][0a21] at `POSITION`. Provide only one keyword
    argument among `PREVIOUS-POSITION` and `VELOCITY`, since the respective other is
    calculated from the provided. If none are provided, assume a velocity of 0.

<a id='x-28VERLET-3APOSITION-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3APARTICLE-29-29'></a>

- [accessor] **POSITION\<-** *PARTICLE* *(:POSITION)*

<a id='x-28VERLET-3AMASS-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3APARTICLE-29-29'></a>

- [accessor] **MASS\<-** *PARTICLE* *(:MASS)*

<a id='x-28VERLET-3AVELOCITY-3C--20FUNCTION-29'></a>

- [function] **VELOCITY\<-** *PARTICLE*

    Return the velocity of `PARTICLE` as a `(SIMPLE-ARRAY SINGLE-FLOAT (3))`.
    
    Please note that this is a best guess at the current velocity, based on the
    forces applied so far in the current frame.

<a id='x-28VERLET-3ASPEED-3C--20FUNCTION-29'></a>

- [function] **SPEED\<-** *PARTICLE*

    Return the speed of `PARTICLE` as a `SINGLE-FLOAT`.
    
    Please note that this is a best guess at the current speed, based on the forces
    applied so far in the current frame.

<a id='x-28VERLET-3AACCELERATE-21-20FUNCTION-29'></a>

- [function] **ACCELERATE!** *PARTICLE DV*

<a id='x-28VERLET-3ADEFLECT-21-20FUNCTION-29'></a>

- [function] **DEFLECT!** *PARTICLE DIRECTION*

    Moves `PARTICLE` to `TARGET` and mirrors the `PARTICLE`'s velocity on the plane
    containing `TARGET` with the change in position as normal.

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3APARTICLE-29-29-29'></a>

- [method] **UPDATE!** *(PARTICLE PARTICLE)*

    Move `PARTICLE` by `DELTA` times the `PARTICLE`s velocity.

<a id='x-28VERLET-3A-40SPRING-20MGL-PAX-3ASECTION-29'></a>

### 2.4 Springs

<a id='x-28VERLET-3ASPRING-20-28VERLET-3AFORCE-29-29'></a>

- [force] **SPRING** *(P1 P2 &OPTIONAL P3 P4) K &OPTIONAL VOLUME*



<a id='x-28VERLET-3A-40LINEAR-SPRING-20MGL-PAX-3ASECTION-29'></a>

#### 2.4.1 Linear Springs

<a id='x-28VERLET-3ASPRING-20CLASS-29'></a>

- [class] **SPRING**

<a id='x-28VERLET-3A-5BSPRING-5D-20FUNCTION-29'></a>

- [function] **\[SPRING\]** *P1 P2 K &KEY (LENGTH (RTG-MATH.VECTOR3:DISTANCE (POSITION\<- P1) (POSITION\<- P2)))*

    Create a [`SPRING`][e296] connecting the [`PARTICLE`][0a21]s `P1` and `P2`, with ideal
    length `LENGTH` and spring constant `K`. `LENGTH` defaults to the distance between `P1`
    and `P2`.

<a id='x-28VERLET-3AK-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASPRING-29-29'></a>

- [accessor] **K\<-** *SPRING* *(:K)*

<a id='x-28VERLET-3ALENGTH-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ASPRING-29-29'></a>

- [accessor] **LENGTH\<-** *SPRING* *(:LENGTH)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3ASPRING-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT SPRING)*

    Exert a length-restoring force on the endpoints of the linear spring.

<a id='x-28VERLET-3A-40AREA-SPRING-20MGL-PAX-3ASECTION-29'></a>

#### 2.4.2 Area Springs

Area springs are hard to predict and may be buggy in unknown ways. Combining
them with standard or volume springs seems to violato conservation of
momentum.

<a id='x-28VERLET-3AAREA-SPRING-20CLASS-29'></a>

- [class] **AREA-SPRING**

<a id='x-28VERLET-3A-5BAREA-SPRING-5D-20FUNCTION-29'></a>

- [function] **\[AREA-SPRING\]** *P1 P2 P3 K &KEY (AREA (/
 (RTG-MATH.VECTOR3:LENGTH
  (RTG-MATH.VECTOR3:CROSS (RTG-MATH.VECTOR3:- (POSITION\<- P1) (POSITION\<- P2))
                          (RTG-MATH.VECTOR3:- (POSITION\<- P1)
                                              (POSITION\<- P3))))
 2.0))*

    Creates an [`AREA-SPRING`][3895] with corners `P1`, `P2` and `P3`, ideal area `AREA`, and
    spring constant `K`. `AREA` defaults to the area enclosed by the supplied
    particles.

<a id='x-28VERLET-3AAREA-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AAREA-SPRING-29-29'></a>

- [accessor] **AREA\<-** *AREA-SPRING* *(:AREA)*

<a id='x-28VERLET-3AK-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AAREA-SPRING-29-29'></a>

- [accessor] **K\<-** *AREA-SPRING* *(:K)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AAREA-SPRING-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT AREA-SPRING)*

    Exert an area-restoring force on the corners of the area spring

<a id='x-28VERLET-3A-40VOLUME-SPRING-20MGL-PAX-3ASECTION-29'></a>

#### 2.4.3 Volume Springs

Volume springs are hard to predict and may be buggy in unknown
ways. Combining them with standard or area springs seems to violate
conservation of momentum.

<a id='x-28VERLET-3AVOLUME-SPRING-20CLASS-29'></a>

- [class] **VOLUME-SPRING**

<a id='x-28VERLET-3A-5BVOLUME-SPRING-5D-20FUNCTION-29'></a>

- [function] **\[VOLUME-SPRING\]** *P1 P2 P3 P4 K &KEY (VOLUME (ABS
 (/
  (RTG-MATH.VECTOR3:DOT (RTG-MATH.VECTOR3:- (POSITION\<- P4) (POSITION\<- P1))
                        (RTG-MATH.VECTOR3:CROSS
                         (RTG-MATH.VECTOR3:- (POSITION\<- P3) (POSITION\<- P1))
                         (RTG-MATH.VECTOR3:- (POSITION\<- P2) (POSITION\<- P1))))
  6)))*

    Create a [`VOLUME-SPRING`][2baa] with corners `P1`, `P2`, `P3` and `P4`, ideal volume `VOLUME`,
    and spring constant `K`. `VOLUME` defaults to the volume enclosed by the given
    particles.

<a id='x-28VERLET-3AVOLUME-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AVOLUME-SPRING-29-29'></a>

- [accessor] **VOLUME\<-** *VOLUME-SPRING* *(:VOLUME)*

<a id='x-28VERLET-3AK-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AVOLUME-SPRING-29-29'></a>

- [accessor] **K\<-** *VOLUME-SPRING* *(:K)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AVOLUME-SPRING-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT VOLUME-SPRING)*

    Exert a volume-restoring force on the corners of the volume spring.

<a id='x-28VERLET-3A-40MODIFICATOR-20MGL-PAX-3ASECTION-29'></a>

### 2.5 Modificators

<a id='x-28VERLET-3AMODIFICATOR-20CLASS-29'></a>

- [class] **MODIFICATOR**

    `MODIFICATOR` objects keep track of other objects to modify in a uniform
    fashion. The `MODIFICATOR` class itself is not meant to be instanciated. An
    inheriting class should implement a method for [`UPDATE!`][028f] to make use of its
    subjects.

<a id='x-28VERLET-3AADD-SUBJECT-20GENERIC-FUNCTION-29'></a>

- [generic-function] **ADD-SUBJECT** *MODIFICATOR SUBJECT*

    Adds a `SUBJECT` to the `MODIFICATOR` for tracking.

<a id='x-28VERLET-3AADD-SUBJECT-20-28METHOD-20NIL-20-28VERLET-3AMODIFICATOR-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **ADD-SUBJECT** *(MODIFICATOR MODIFICATOR) (SUBJECT PARTICLE)*

<a id='x-28VERLET-3AREMOVE-SUBJECT-20GENERIC-FUNCTION-29'></a>

- [generic-function] **REMOVE-SUBJECT** *MODIFICATOR SUBJECT*

    Remove a `SUBJECT` to the `MODIFICATOR` so it won't be tracked anymore.

<a id='x-28VERLET-3AREMOVE-SUBJECT-20-28METHOD-20NIL-20-28VERLET-3AMODIFICATOR-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **REMOVE-SUBJECT** *(MODIFICATOR MODIFICATOR) (SUBJECT PARTICLE)*

<a id='x-28VERLET-3A-40DAMPER-20MGL-PAX-3ASECTION-29'></a>

#### 2.5.1 Damper

<a id='x-28VERLET-3ADAMPER-20-28VERLET-3AFORCE-29-29'></a>

- [force] **DAMPER** *(&REST INDICE) D*



<a id='x-28VERLET-3ADAMPER-20CLASS-29'></a>

- [class] **DAMPER** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BDAMPER-5D-20FUNCTION-29'></a>

- [function] **\[DAMPER\]** *D &REST PARTICLES*

<a id='x-28VERLET-3AD-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ADAMPER-29-29'></a>

- [accessor] **D\<-** *DAMPER* *(:D)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3ADAMPER-29-29-29'></a>

- [method] **UPDATE!** *(DAMPER DAMPER)*

<a id='x-28VERLET-3A-40GRAVITY-20MGL-PAX-3ASECTION-29'></a>

#### 2.5.2 Gravity

<a id='x-28VERLET-3A-40GLOBAL-GRAVITY-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.2.1 Global Gravity

<a id='x-28VERLET-3AGLOBAL-GRAVITY-20-28VERLET-3AFORCE-29-29'></a>

- [force] **GLOBAL-GRAVITY** *X Y Z*



<a id='x-28VERLET-3AGLOBAL-GRAVITY-20CLASS-29'></a>

- [class] **GLOBAL-GRAVITY** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BGLOBAL-GRAVITY-5D-20FUNCTION-29'></a>

- [function] **\[GLOBAL-GRAVITY\]** *GRAVITY*

    Creates an instance of [`GLOBAL-GRAVITY`][b2cc] which accelerates all its
    subjects by `GRAVITY` on each frame.

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AGLOBAL-GRAVITY-29-29-29'></a>

- [method] **UPDATE!** *(GRAVITY GLOBAL-GRAVITY)*

    Accelerate the subjects of `GRAVITY` by the gravity vector.

<a id='x-28VERLET-3A-40MUTUAL-GRAVITY-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.2.2 Mutual Gravity

<a id='x-28VERLET-3AMUTUAL-GRAVITY-20-28VERLET-3AFORCE-29-29'></a>

- [force] **MUTUAL-GRAVITY**



<a id='x-28VERLET-3AMUTUAL-GRAVITY-20CLASS-29'></a>

- [class] **MUTUAL-GRAVITY** *[MODIFICATOR][cafa]*

    Applies gravitational force to the tracked particles according to the mass
    of the other tracked particles.

<a id='x-28VERLET-3A-5BMUTUAL-GRAVITY-5D-20FUNCTION-29'></a>

- [function] **\[MUTUAL-GRAVITY\]** 

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AMUTUAL-GRAVITY-29-29-29'></a>

- [method] **UPDATE!** *(GRAVITY MUTUAL-GRAVITY)*

<a id='x-28VERLET-3A-40FIELD-GRAVITY-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.2.3 Field Gravity

<a id='x-28VERLET-3AFIELD-GRAVITY-20-28VERLET-3AFORCE-29-29'></a>

- [force] **FIELD-GRAVITY** *POSITION-VAR &BODY BODY*



<a id='x-28VERLET-3AFIELD-GRAVITY-20CLASS-29'></a>

- [class] **FIELD-GRAVITY** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BFIELD-GRAVITY-5D-20FUNCTION-29'></a>

- [function] **\[FIELD-GRAVITY\]** *FIELD*

    Creates [`FIELD-GRAVITY`][8a18] with gravity field `FIELD`. `FIELD` must map
    positions `(SIMPLE-ARRAY SINGLE-FLOAT (3))`s to gravity vectors
    `(SIMPLE-ARRAY SINGLE-FLOAT (3))`.

<a id='x-28VERLET-3AFIELD-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AFIELD-GRAVITY-29-29'></a>

- [accessor] **FIELD\<-** *FIELD-GRAVITY* *(:FIELD)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AFIELD-GRAVITY-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT FIELD-GRAVITY)*

<a id='x-28VERLET-3A-40CONSTRAINT-20MGL-PAX-3ASECTION-29'></a>

#### 2.5.3 Constraints

<a id='x-28VERLET-3ACONSTRAINT-20CLASS-29'></a>

- [class] **CONSTRAINT** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3AVIOLATED-3F-20GENERIC-FUNCTION-29'></a>

- [generic-function] **VIOLATED?** *CONSTRAINT PARTICLE*

    Returns `NIL` when the constraint is fulfilled and a truthy value
    otherwise.

<a id='x-28VERLET-3ADEFLECTION-VECTOR-20GENERIC-FUNCTION-29'></a>

- [generic-function] **DEFLECTION-VECTOR** *CONSTRAINT PARTICLE*

    Returns the cange in position for `PARTICLE` to obey `CONSTRAINT` again.

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3ACONSTRAINT-29-29-29'></a>

- [method] **UPDATE!** *(CONSTRAINT CONSTRAINT)*

    Deflects all subjects violating the constraint as per [`VIOLATED?`][8eec] by the vector
    defined by [`DEFLECTION-VECTOR`][0d80].

<a id='x-28VERLET-3A-40PLANE-CONSTRAINT-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.3.1 Plane Constraints

<a id='x-28VERLET-3APLANE-CONSTRAINT-20-28VERLET-3AFORCE-29-29'></a>

- [force] **PLANE-CONSTRAINT** *(X Y Z) (NX NY NZ)*



<a id='x-28VERLET-3APLANE-CONSTRAINT-20CLASS-29'></a>

- [class] **PLANE-CONSTRAINT** *[CONSTRAINT][ea40]*

<a id='x-28VERLET-3A-5BPLANE-CONSTRAINT-5D-20FUNCTION-29'></a>

- [function] **\[PLANE-CONSTRAINT\]** *POINT NORMAL*

    Creates a [`PLANE-CONSTRAINT`][f48f] restricting all subjects to a half-space
    defined by the plane containing `POINT` with normal `NORMAL`. The usable half-space
    is the closed half-space that `NORMAL` is pointing into.

<a id='x-28VERLET-3AVIOLATED-3F-20-28METHOD-20NIL-20-28VERLET-3APLANE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **VIOLATED?** *(CONSTRAINT PLANE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3ADEFLECTION-VECTOR-20-28METHOD-20NIL-20-28VERLET-3APLANE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **DEFLECTION-VECTOR** *(CONSTRAINT PLANE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3A-40SPHERE-CONSTRAINT-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.3.2 Sphere Constraints

<a id='x-28VERLET-3ASPHERE-CONSTRAINT-20-28VERLET-3AFORCE-29-29'></a>

- [force] **SPHERE-CONSTRAINT** *(CX CY CZ) R &OPTIONAL OUTER-WALL?*



<a id='x-28VERLET-3ASPHERE-CONSTRAINT-20CLASS-29'></a>

- [class] **SPHERE-CONSTRAINT** *[CONSTRAINT][ea40]*

<a id='x-28VERLET-3A-5BSPHERE-CONSTRAINT-5D-20FUNCTION-29'></a>

- [function] **\[SPHERE-CONSTRAINT\]** *CENTER RADIUS &OPTIONAL OUTER-WALL?*

    Creates a [`SPHERE-CONSTRAINT`][61b9] with center `CENTER` and radius
    `RADIUS`. `CENTER` is a `(SIMPLE-ARRAY SINGLE-FLOAT (3))`, `RADIUS` a
    `SINGLE-FLOAT`. When `OUTER-WALL?` is truthy, movement is restricted to the closed
    ball described by the sphere, otherwise movement is constricted to the outside
    of the open ball described by the sphere.

<a id='x-28VERLET-3AVIOLATED-3F-20-28METHOD-20NIL-20-28VERLET-3ASPHERE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **VIOLATED?** *(CONSTRAINT SPHERE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3ADEFLECTION-VECTOR-20-28METHOD-20NIL-20-28VERLET-3ASPHERE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **DEFLECTION-VECTOR** *(CONSTRAINT SPHERE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3A-40TRIANGLE-CONSTRAINT-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.3.3 Triangle Constraints

<a id='x-28VERLET-3ATRIANGLE-CONSTRAINT-20-28VERLET-3AFORCE-29-29'></a>

- [force] **TRIANGLE-CONSTRAINT** *(X1 Y1 Z1) (X2 Y2 Z2) (X3 Y3 Z3)*



<a id='x-28VERLET-3ATRIANGLE-CONSTRAINT-20CLASS-29'></a>

- [class] **TRIANGLE-CONSTRAINT** *[CONSTRAINT][ea40]*

<a id='x-28VERLET-3A-5BTRIANGLE-CONSTRAINT-5D-20FUNCTION-29'></a>

- [function] **\[TRIANGLE-CONSTRAINT\]** *P1 P2 P3*

    Create a [`TRIANGLE-CONSTRAINT`][f965] preventing particles moving through the
    triangle enclosed by `P1`, `P2`, and `P3`

<a id='x-28VERLET-3AVIOLATED-3F-20-28METHOD-20NIL-20-28VERLET-3ATRIANGLE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **VIOLATED?** *(CONSTRAINT TRIANGLE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3ADEFLECTION-VECTOR-20-28METHOD-20NIL-20-28VERLET-3ATRIANGLE-CONSTRAINT-20VERLET-3APARTICLE-29-29-29'></a>

- [method] **DEFLECTION-VECTOR** *(CONSTRAINT TRIANGLE-CONSTRAINT) (PARTICLE PARTICLE)*

<a id='x-28VERLET-3A-40FRICTION-20MGL-PAX-3ASECTION-29'></a>

#### 2.5.4 Friction

<a id='x-28VERLET-3AFRICTION-21-20FUNCTION-29'></a>

- [function] **FRICTION!** *PARTICLE FRICTION DELTA*

    Reduce the speed of `PARTICLE` by FRICTION·DELTA to a minimum of speed 0. This
    means that the `PARTICLE`'s velocity's direction is never reversed by this
    function. Modifies `PARTICLE`.

<a id='x-28VERLET-3A-40GLOBAL-FRICTION-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.4.1 Global Friction

<a id='x-28VERLET-3AGLOBAL-FRICTION-20-28VERLET-3AFORCE-29-29'></a>

- [force] **GLOBAL-FRICTION** *F*



<a id='x-28VERLET-3AGLOBAL-FRICTION-20CLASS-29'></a>

- [class] **GLOBAL-FRICTION** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BGLOBAL-FRICTION-5D-20FUNCTION-29'></a>

- [function] **\[GLOBAL-FRICTION\]** *FRICTION*

<a id='x-28VERLET-3AFRICTION-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AGLOBAL-FRICTION-29-29'></a>

- [accessor] **FRICTION\<-** *GLOBAL-FRICTION* *(:FRICTION)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3AGLOBAL-FRICTION-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT GLOBAL-FRICTION)*

    Apply the global friction to all subjects.

<a id='x-28VERLET-3A-40FIELD-FRICTION-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.4.2 Field Friction

<a id='x-28VERLET-3AFIELD-FRICTION-20-28VERLET-3AFORCE-29-29'></a>

- [force] **FIELD-FRICTION** *POSITION-VAR &BODY BODY*



<a id='x-28VERLET-3AFIELD-FRICTION-20CLASS-29'></a>

- [class] **FIELD-FRICTION** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BFIELD-FRICTION-5D-20FUNCTION-29'></a>

- [function] **\[FIELD-FRICTION\]** *FIELD*

    Creates [`FIELD-FRICTION`][2dfb] with friction field `FIELD`. `FIELD` must map 3D
    positions to friction values.

<a id='x-28VERLET-3AFIELD-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3AFIELD-FRICTION-29-29'></a>

- [accessor] **FIELD\<-** *FIELD-FRICTION* *(:FIELD)*

<a id='x-28VERLET-3A-40CONSTRAINT-FRICTION-20MGL-PAX-3ASECTION-29'></a>

##### 2.5.4.3 Constraint Friction

<a id='x-28VERLET-3ACONSTRAINT-FRICTION-20CLASS-29'></a>

- [class] **CONSTRAINT-FRICTION** *[MODIFICATOR][cafa]*

<a id='x-28VERLET-3A-5BCONSTRAINT-FRICTION-5D-20FUNCTION-29'></a>

- [function] **\[CONSTRAINT-FRICTION\]** *CONSTRAINT FRICTION*

    Creates [`CONSTRAINT-FRICTION`][1d66] which applies friction `FRICTION` to subjects on
    collision with `CONSTRAINT`. Note that the subject of the [`CONSTRAINT-FRICTION`][1d66]
    instance and of the constraint need not necessarily be the same.

<a id='x-28VERLET-3AFRICTION-3C--20-28MGL-PAX-3AACCESSOR-20VERLET-3ACONSTRAINT-FRICTION-29-29'></a>

- [accessor] **FRICTION\<-** *CONSTRAINT-FRICTION* *(:FRICTION)*

<a id='x-28VERLET-3AUPDATE-21-20-28METHOD-20NIL-20-28VERLET-3ACONSTRAINT-FRICTION-29-29-29'></a>

- [method] **UPDATE!** *(OBJECT CONSTRAINT-FRICTION)*

  [028f]: #x-28VERLET-3AUPDATE-21-20GENERIC-FUNCTION-29 "(VERLET:UPDATE! GENERIC-FUNCTION)"
  [07c1]: #x-28VERLET-3A-40PARTICLE-20MGL-PAX-3ASECTION-29 "Particles"
  [0a21]: #x-28VERLET-3APARTICLE-20CLASS-29 "(VERLET:PARTICLE CLASS)"
  [0d80]: #x-28VERLET-3ADEFLECTION-VECTOR-20GENERIC-FUNCTION-29 "(VERLET:DEFLECTION-VECTOR GENERIC-FUNCTION)"
  [0ebf]: #x-28VERLET-3A-40PLANE-CONSTRAINT-20MGL-PAX-3ASECTION-29 "Plane Constraints"
  [1a80]: #x-28VERLET-3A-40TRIANGLE-CONSTRAINT-20MGL-PAX-3ASECTION-29 "Triangle Constraints"
  [1bb9]: #x-28VERLET-3A-40FIELD-FRICTION-20MGL-PAX-3ASECTION-29 "Field Friction"
  [1d66]: #x-28VERLET-3ACONSTRAINT-FRICTION-20CLASS-29 "(VERLET:CONSTRAINT-FRICTION CLASS)"
  [1e57]: #x-28VERLET-3A-40CONSTRAINT-FRICTION-20MGL-PAX-3ASECTION-29 "Constraint Friction"
  [27d2]: #x-28VERLET-3A-5BPARTICLE-5D-20FUNCTION-29 "(VERLET:[PARTICLE] FUNCTION)"
  [2baa]: #x-28VERLET-3AVOLUME-SPRING-20CLASS-29 "(VERLET:VOLUME-SPRING CLASS)"
  [2dfb]: #x-28VERLET-3AFIELD-FRICTION-20CLASS-29 "(VERLET:FIELD-FRICTION CLASS)"
  [376d]: #x-28-22verlet-22-20ASDF-2FSYSTEM-3ASYSTEM-29 "(\"verlet\" ASDF/SYSTEM:SYSTEM)"
  [3895]: #x-28VERLET-3AAREA-SPRING-20CLASS-29 "(VERLET:AREA-SPRING CLASS)"
  [4857]: #x-28VERLET-3ABUILD-SYSTEM-20-28MGL-PAX-3AMACRO-29-29 "(VERLET:BUILD-SYSTEM (MGL-PAX:MACRO))"
  [55de]: #x-28VERLET-3ADEFORCE-20-28MGL-PAX-3AMACRO-29-29 "(VERLET:DEFORCE (MGL-PAX:MACRO))"
  [6115]: #x-28VERLET-3A-40CONSTRAINT-20MGL-PAX-3ASECTION-29 "Constraints"
  [61b9]: #x-28VERLET-3ASPHERE-CONSTRAINT-20CLASS-29 "(VERLET:SPHERE-CONSTRAINT CLASS)"
  [6bfc]: #x-28VERLET-3A-40LINEAR-SPRING-20MGL-PAX-3ASECTION-29 "Linear Springs"
  [6c05]: #x-28VERLET-3A-40VOLUME-SPRING-20MGL-PAX-3ASECTION-29 "Volume Springs"
  [6f25]: #x-28VERLET-3A-40INTERFACE-20MGL-PAX-3ASECTION-29 "Interface"
  [7130]: #x-28VERLET-3A-40FORCE-20MGL-PAX-3ASECTION-29 "Force"
  [7a53]: #x-28VERLET-3A-40SPHERE-CONSTRAINT-20MGL-PAX-3ASECTION-29 "Sphere Constraints"
  [7ba3]: #x-28VERLET-3A-40GLOBAL-FRICTION-20MGL-PAX-3ASECTION-29 "Global Friction"
  [881d]: #x-28VERLET-3A-40DAMPER-20MGL-PAX-3ASECTION-29 "Damper"
  [8a18]: #x-28VERLET-3AFIELD-GRAVITY-20CLASS-29 "(VERLET:FIELD-GRAVITY CLASS)"
  [8eec]: #x-28VERLET-3AVIOLATED-3F-20GENERIC-FUNCTION-29 "(VERLET:VIOLATED? GENERIC-FUNCTION)"
  [995d]: #x-28VERLET-3A-40FRICTION-20MGL-PAX-3ASECTION-29 "Friction"
  [a429]: #x-28VERLET-3ASYSTEM-20CLASS-29 "(VERLET:SYSTEM CLASS)"
  [b17f]: #x-28VERLET-3A-40SPRING-20MGL-PAX-3ASECTION-29 "Springs"
  [b2cc]: #x-28VERLET-3AGLOBAL-GRAVITY-20CLASS-29 "(VERLET:GLOBAL-GRAVITY CLASS)"
  [b8f0]: #x-28VERLET-3A-40GLOBAL-GRAVITY-20MGL-PAX-3ASECTION-29 "Global Gravity"
  [bb37]: #x-28VERLET-3A-40PHYSICS-SYSTEM-20MGL-PAX-3ASECTION-29 "Physics System"
  [c236]: #x-28VERLET-3ASTEP-21-20FUNCTION-29 "(VERLET:STEP! FUNCTION)"
  [cafa]: #x-28VERLET-3AMODIFICATOR-20CLASS-29 "(VERLET:MODIFICATOR CLASS)"
  [dda4]: #x-28VERLET-3A-40AREA-SPRING-20MGL-PAX-3ASECTION-29 "Area Springs"
  [e296]: #x-28VERLET-3ASPRING-20CLASS-29 "(VERLET:SPRING CLASS)"
  [e2ab]: #x-28VERLET-3A-40FIELD-GRAVITY-20MGL-PAX-3ASECTION-29 "Field Gravity"
  [ea40]: #x-28VERLET-3ACONSTRAINT-20CLASS-29 "(VERLET:CONSTRAINT CLASS)"
  [eb83]: #x-28VERLET-3A-40MODIFICATOR-20MGL-PAX-3ASECTION-29 "Modificators"
  [f48f]: #x-28VERLET-3APLANE-CONSTRAINT-20CLASS-29 "(VERLET:PLANE-CONSTRAINT CLASS)"
  [f965]: #x-28VERLET-3ATRIANGLE-CONSTRAINT-20CLASS-29 "(VERLET:TRIANGLE-CONSTRAINT CLASS)"
  [fbfc]: #x-28VERLET-3A-40MUTUAL-GRAVITY-20MGL-PAX-3ASECTION-29 "Mutual Gravity"
  [fd5b]: #x-28VERLET-3A-40GRAVITY-20MGL-PAX-3ASECTION-29 "Gravity"
