(in-package #:verlet)

(defsection @global-friction (:title "Global Friction")
  (global-friction force)
  (global-friction class)
  ([global-friction] function)
  (friction<- (accessor global-friction))
  (update! (method () (global-friction))))

(defclass global-friction (modificator)
  ((%friction :type (single-float (0.0))
              :initarg :friction
              :accessor friction<-)))

(defun [global-friction] (friction)
  (make-instance 'global-friction
                 :friction friction))

(defmethod update! ((object global-friction)
                    &optional
                      (delta 1.0))
  "Apply the global friction to all subjects."
  (declare (type (single-float (0.0))
                 delta))
  (bind ((friction (friction<- object)))
    (do-set (particle (subjects<- object))
      (friction! particle friction delta))))

(deforce global-friction :velocity-dependent-force (f)
  `(bind ((fr ([global-friction] ,f)))
     (setf (subjects<- fr)
           (convert 'set particles))
     fr))
