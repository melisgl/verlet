(in-package #:verlet)

(defsection @constraint-friction (:title "Constraint Friction")
  (constraint-friction class)
  ([constraint-friction] function)
  (friction<- (accessor constraint-friction))
  (update! (method () (constraint-friction))))

(defclass constraint-friction (modificator)
  ((%constraint :type constraint
                :initarg :constraint
                :reader constraint<-)
   (%friction :type (single-float (0.0))
              :initarg :friction
              :accessor friction<-)))

(defun [constraint-friction] (constraint friction)
  "Creates CONSTRAINT-FRICTION which applies friction FRICTION to subjects on
 collision with CONSTRAINT. Note that the subject of the CONSTRAINT-FRICTION
 instance and of the constraint need not necessarily be the same."
  (make-instance 'constraint-friction
                 :constraint constraint
                 :friction friction))

(defmethod update! ((object constraint-friction)
                    &optional
                      (delta 1.0))
  (declare (type (single-float (0.0))
                 delta))
  (bind ((constraint (constraint<- object))
         (friction (friction<- object)))
    (do-set (particle (subjects<- object))
      (when (violated? constraint particle)
        (friction! particle friction delta)))))

;;;; TODO:
;;;;  Figure out how to define a force for this kind of friction
