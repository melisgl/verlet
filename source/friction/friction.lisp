(in-package #:verlet)

(defsection @friction (:title "Friction")
  (friction! function)
  (@global-friction section)
  (@field-friction section)
  (@constraint-friction section))

(defun friction! (particle friction delta)
  "Reduce the speed of PARTICLE by FRICTION·DELTA to a minimum of speed 0. This
 means that the PARTICLE's velocity's direction is never reversed by this
 function. Modifies PARTICLE."
  (set=> (:var p (previous-position<- particle))
    (:var v (temp-velocity<- particle))
    (/ (* friction delta)
       (v3:length %))
    (min % 1.0)
    (v3:*s v %)
    (v3:+ p %))
  particle)
