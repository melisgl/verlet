(in-package #:verlet)

(defsection @damper (:title "Damper")
  (damper force)
  (damper class)
  ([damper] function)
  (d<- (accessor damper))
  (update! (method () (damper))))

(defclass damper (modificator)
  ((%d :type (single-float (0.0))
       :initarg :d
       :accessor d<-)))

(defun [damper] (d &rest particles)
  (bind ((damper (make-instance 'damper
                                :d (float d 1.0))))
    (loop
      :for particle :in particles
      :do
         (add-subject damper particle)
      :finally (return damper))))

(defmethod update! ((damper damper)
                    &optional
                      delta)
  (declare (ignore delta))
  (bind ((particles (subjects<- damper))
         (center (center-of-mass particles))
         (d (d<- damper)))
    (do-set (particle particles)
      (bind ((pd (v3:normalize (v3:- (position<- particle)
                                     center)))
             (others-dir (reduce (lambda (acc p)
                                   (v3:- acc (temp-velocity<- p)))
                                 (less particles particle)
                                 :initial-value (temp-velocity<- particle))))
        (apply-force! particle (v3:*s pd (- (* (v3:dot pd others-dir)
                                               d))))))))

(deforce damper :velocity-dependent-force ((&rest indice)
                                           d)
  `(apply #'[damper]
          ,d (mapcar (lambda (index)
                       (nth index particles))
                     (list ,@indice))))
