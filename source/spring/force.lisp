(in-package #:verlet)

(deforce spring :force ((p1 p2 &optional p3 p4)
                        k &optional volume)
  `(bind ((p1 (nth ,p1 particles))
          (p2 (nth ,p2 particles))
          (p3 (and ,p3 (nth ,p3 particles)))
          (p4 (and ,p4 (nth ,p4 particles))))
     (if ,volume
         (cond
           (p4
            ([volume-spring] p1 p2 p3 p3 ,k
                             :volume ,volume))
           (p3
            ([area-spring] p1 p2 p3 ,k
                           :area ,volume))
           (t
            ([spring] p1 p2 ,k
                      :length ,volume)))
         (cond
           (p4
            ([volume-spring] p1 p2 p3 p3 ,k))
           (p3
            ([area-spring] p1 p2 p3 ,k))
           (t
            ([spring] p1 p2 ,k))))))
