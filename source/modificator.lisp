(in-package #:verlet)

(defsection @modificator (:title "Modificators")
  (modificator class)
  (add-subject generic-function)
  (add-subject (method () (modificator particle)))
  (remove-subject generic-function)
  (remove-subject (method () (modificator particle)))
  (@damper section)
  (@gravity section)
  (@constraint section)
  (@friction section))

(defclass modificator ()
  ((%subjects :type set
              :initform (set)
              :accessor subjects<-))
  (:documentation
   "MODIFICATOR objects keep track of other objects to modify in a uniform
 fashion. The MODIFICATOR class itself is not meant to be instanciated. An
 inheriting class should implement a method for UPDATE! to make use of its
 subjects."))

(defgeneric add-subject (modificator subject)
  (:documentation
   "Adds a SUBJECT to the MODIFICATOR for tracking.")
  (:method ((modificator modificator)
            (subject particle))
    (set=> (subjects<- modificator)
      (with % subject))))

(defgeneric remove-subject (modificator subject)
  (:documentation
   "Remove a SUBJECT to the MODIFICATOR so it won't be tracked anymore.")
  (:method ((modificator modificator)
            (subject particle))
    (set=> (subjects<- modificator)
      (less % subject))))
