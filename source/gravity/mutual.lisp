(in-package #:verlet)

(defsection @mutual-gravity (:title "Mutual Gravity")
  (mutual-gravity force)
  (mutual-gravity class)
  ([mutual-gravity] function)
  (update! (method () (mutual-gravity))))

(defclass mutual-gravity (modificator)
  ()
  (:documentation
   "Applies gravitational force to the tracked particles according to the mass
 of the other tracked particles."))

(defun [mutual-gravity] ()
  (make-instance 'mutual-gravity))

(defmethod update! ((gravity mutual-gravity)
                    &optional
                      delta)
  (declare (ignore delta))
  (bind ((subjects (subjects<- gravity)))
    (do-set (particle subjects)
      (bind ((position (position<- particle))
             (m₁ (mass<- particle)))
        (do-set (other-particle (less subjects particle))
          (bind ((dir (v3:- (position<- other-particle)
                            position)))
            (apply-force! particle (v3:*s (v3:normalize dir)
                                          (/ (* m₁ (mass<- other-particle))
                                             (v3:dot dir dir))))))))))

(deforce mutual-gravity :force ()
  `(bind ((grav ([mutual-gravity])))
     (setf (subjects<- grav)
           (convert 'set particles))
     grav))
