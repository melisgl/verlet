(in-package #:verlet)

(defsection @global-gravity (:title "Global Gravity")
  (global-gravity force)
  (global-gravity class)
  ([global-gravity] function)
  (update! (method () (global-gravity))))

(defclass global-gravity (modificator)
  ((%gravity :type (simple-array single-float (3))
             :initarg :gravity
             :accessor gravity<-)))

(defun [global-gravity] (gravity)
  "Creates an instance of [GLOBAL-GRAVITY][class] which accelerates all its
 subjects by GRAVITY on each frame."
  (make-instance 'global-gravity
                 :gravity gravity))

(defmethod update! ((gravity global-gravity)
                    &optional
                      delta)
  "Accelerate the subjects of GRAVITY by the gravity vector."
  (declare (ignore delta))
  (do-set (particle (subjects<- gravity))
    (accelerate! particle (gravity<- gravity))))

(deforce global-gravity :force (x y z)
  `(bind ((grav ([global-gravity] (v! ,x ,y ,z))))
     (setf (subjects<- grav)
           (convert 'set particles))
     grav))
