(in-package #:verlet)

(defsection @field-gravity (:title "Field Gravity")
  (field-gravity force)
  (field-gravity class)
  ([field-gravity] function)
  (field<- (accessor field-gravity))
  (update! (method () (field-gravity))))

(defclass field-gravity (modificator)
  ((%field :type (function ((simple-array single-float (3)))
                           (simple-array single-float (3)))
           :initarg :field
           :accessor field<-)))

(defun [field-gravity] (field)
  "Creates [FIELD-GRAVITY][class] with gravity field FIELD. FIELD must map
 positions `(SIMPLE-ARRAY SINGLE-FLOAT (3))`s to gravity vectors
 `(SIMPLE-ARRAY SINGLE-FLOAT (3))`." 
  (make-instance 'field-gravity
                 :field field))

(defmethod update! ((object field-gravity)
                    &optional
                      delta)
  (declare (ignore delta))
  (bind ((field (field<- object)))
    (do-set (particle (subjects<- object))
      (accelerate! particle (funcall field (position<- particle))))))

(deforce field-gravity :velocity-dependent-force (position-var &body body)
  `(bind ((grav ([field-gravity] (lambda (,position-var)
                                   ,@body))))
     (setf (subjects<- grav)
           (convert 'set particles))
     grav))
