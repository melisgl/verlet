(in-package #:verlet)

(defsection @physics-system (:title "Physics System")
  (system class)
  ([system] function)
  (build-system macro)
  (particles<- (accessor system))
  (forces<- (accessor system))
  (velocity-dependent-forces<- (accessor system))
  (collisions<- (accessor system))
  (constraints<- (accessor system))
  (step! function))

(defclass system ()
  ((%particles :type set
               :initform (set)
               :accessor particles<-)
   (%forces :type set
            :initform (set)
            :accessor forces<-)
   (%velocity-dependent-forces :type set
                               :initform (set)
                               :accessor velocity-dependent-forces<-)
   (%collisions :type set
                :initform (set)
                :accessor collisions<-)
   (%constraints :type set
                 :initform (set)
                 :accessor constraints<-)))

(defun [system] ()
  (make-instance 'system))

(defun step! (system delta
              &key
                (resolve-steps 1))
  "Updates a physical SYSTEM by timestep DELTA.

 1. Apply all forces not dependent on the velocity of particles (:FORCE in the
    DEFORCE macro)
 2. Calculate forces depending on velocities of particles, using the memoized
    velocities so far (:VELOCITY-DEPENDENT-FORCE in the DEFORCE macro)
 3. Repeat RESOLVE-STEPS times:
    1. Resolve all static constraints (:CONSTRAINT in the DEFORCE macro)
    2. Resolve collisions (:COLLISION in the DEFORCE macro)

 Please note after STEP! returns, no collisions should be happening, but static
 constraints might be violated."
  (do-set (force (forces<- system))
    (update! force delta))
  (do-set (particle (particles<- system))
    (setf (temp-velocity<- particle)
          (velocity<- particle)))
  (do-set (force (velocity-dependent-forces<- system))
    (update! force delta))
  (do-set (particle (particles<- system))
    (update! particle))
  (loop
    :for _ :below resolve-steps
    :do
       (do-set (constraint (constraints<- system))
         (update! constraint delta))
       (do-set (collision-set (collisions<- system))
         (update! collision-set delta))))

(defmacro build-system ((&rest particles)
                        &body forces)
  "Build up a standard physics SYSTEM using defined forces.

 PARTICLES is a list with items of the form
 `((X Y Z) &KEY (MASS 1.0) PREVIOUS-POSITION VELOCITY)`.
 The list at the beginning describes the position of the particle, for the
 meaning of the keyword arguments reference the `[PARTICLE]` function.

 FORCES is a list of force descriptors, as defined by the DEFORCE macro. The
 respective forces are going to be added to the final system."
  (bind ((g!system (gensym "system"))
         (g!inline-macro (gensym "inline-macro")))
    `(bind ((particles ())
            (,g!system ([system])))
       ,@(loop
           :for ((x y z)
                 . args)
             :in particles
           :collect `(push ([particle] (v! ,x ,y ,z)
                                       ,@args)
                           particles))
       (set=> (particles<- ,g!system)
         (convert 'set particles))
       (set=> particles
         (nreverse %))
       ,@(loop
           :for (name . args)
             :in forces
           :collect (bind (((:values mode lambda-list body)
                            (find-force name)))
                      `(macrolet
                           ((,g!inline-macro (,@lambda-list)
                              ,@body))
                         (set=> (,(ecase mode
                                    (:force 'forces<-)
                                    (:velocity-dependent-force
                                     'velocity-dependent-forces<-)
                                    (:constraint 'constraints<-)
                                    (:collision 'collisions<-))
                                 ,g!system)
                           (with % (,g!inline-macro ,@args))))))
       ,g!system)))
