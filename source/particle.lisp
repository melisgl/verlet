(in-package #:verlet)

(defsection @particle (:title "Particles")
  (particle class)
  ([particle] function)
  (position<- (accessor particle))
  (mass<- (accessor particle))
  (velocity<- function)
  (speed<- function)
  (accelerate! function)
  (deflect! function)
  (update! (method () (particle))))

(defclass particle ()
  ((%position :type (simple-array single-float (3))
              :initarg :position
              :accessor position<-)
   (%acceleration :type (simple-array single-float (3))
                  :initform (v! 0 0 0)
                  :accessor acceleration<-)
   (%temp-velocity :type (simple-array single-float (3))
                   :initform (v! 0 0 0)
                   :accessor temp-velocity<-)
   (%previous-position :type (simple-array single-float (3))
                       :initarg :previous-position
                       :accessor previous-position<-)
   (%previous-delta :type (real (0))
                    :initform 1.0
                    :accessor previous-delta<-)
   (%actual-previous-position :type (simple-array single-float (3))
                              :initarg :actual-previous-position
                              :accessor actual-previous-position<-)
   (%mass :type (single-float (0.0))
          :initarg :mass
          :accessor mass<-)))

(defmethod print-object ((object particle)
                         stream)
  (format stream "([PARTICLE] ~A ~:_~A)"
          (position<- object)
          (previous-position<- object)))

(defun [particle] (position
                   &key
                     (mass 1.0)
                     previous-position
                     velocity)
  "Returns an instance of class PARTICLE at POSITION. Provide only one keyword
 argument among PREVIOUS-POSITION and VELOCITY, since the respective other is
 calculated from the provided. If none are provided, assume a velocity of 0."
  (assert (not (and previous-position velocity)))
  (make-instance 'particle
                 :position position
                 :previous-position (or previous-position
                                        (and velocity (v3:- position velocity))
                                        position)
                 :actual-previous-position position
                 :mass mass))

(defmethod position<- ((object simple-array))
  object)

(defun next-position (particle delta)
  (bind ((p (position<- particle))
         (pdelta (previous-delta<- particle)))
    (v3:+ p (v3:*s (v3:- p (previous-position<- particle))
                   (/ delta pdelta))
          (v3:*s (acceleration<- particle)
                 (* delta (/ (+ delta pdelta)
                             2))))))

(defun velocity<- (particle)
  "Return the velocity of PARTICLE as a `(SIMPLE-ARRAY SINGLE-FLOAT (3))`.

 Please note that this is a best guess at the current velocity, based on the
 forces applied so far in the current frame."
  (bind ((delta (previous-delta<- particle)))
    (v3:/s (v3:- (next-position particle delta)
                 (previous-position<- particle))
           (* 2 delta))))

(defun speed<- (particle)
  "Return the speed of PARTICLE as a SINGLE-FLOAT.

 Please note that this is a best guess at the current speed, based on the forces
 applied so far in the current frame."
  (v3:length (velocity<- particle)))

(defun center-of-mass (particles)
  (bind ((sum (v! 0 0 0))
         (mass 0.0))
    (do-set (particle particles)
      (v3:incf sum (v3:*s (position<- particle)
                          (mass<- particle)))
      (incf mass (mass<- particle)))
    (v3:/s sum mass)))

(defun accelerate! (particle dv)
  (set=> (acceleration<- particle)
    (v3:+ % dv))
  particle)

(defun apply-force! (particle force)
  (accelerate! particle (v3:/s force (mass<- particle))))

(defun deflect! (particle direction)
  "Moves PARTICLE to TARGET and mirrors the PARTICLE's velocity on the plane
 containing TARGET with the change in position as normal."
  (bind ((target (v3:+ (position<- particle)
                       direction))
         (velocity (v3:- (position<- particle)
                         (previous-position<- particle))))
    (setf (position<- particle)
          target)
    (setf (previous-position<- particle)
          (v3:- target (mirror velocity (v! 0 0 0)
                               (v3:normalize direction))))))

(defgeneric update! (object &optional delta))

(defmethod update! ((particle particle)
                    &optional
                      (delta 1.0))
  "Move PARTICLE by DELTA times the `PARTICLE`s velocity."
  (declare (type single-float delta))
  (bind ((pos (position<- particle)))
    (set=> (position<- particle)
      (next-position particle delta))
    (setf (actual-previous-position<- particle)
          pos)
    (setf (previous-position<- particle)
          pos)
    (setf (acceleration<- particle)
          (v! 0 0 0))
    (setf (previous-delta<- particle)
          delta))
  particle)
