(in-package #:verlet)

(defsection @triangle-constraint (:title "Triangle Constraints")
  (triangle-constraint force)
  (triangle-constraint class)
  ([triangle-constraint] function)
  (violated? (method () (triangle-constraint particle)))
  (deflection-vector (method () (triangle-constraint particle))))

(defclass triangle-constraint (constraint)
  ((%p1 :type (simple-array single-float (3))
        :initarg :p1
        :reader p1<-)
   (%p2 :type (simple-array single-float (3))
        :initarg :p2
        :reader p2<-)
   (%p3 :type (simple-array single-float (3))
        :initarg :p3
        :reader p3<-)))

(defun [triangle-constraint] (p1 p2 p3)
  "Create a [TRIANGLE-CONSTRAINT][class] preventing particles moving through the
 triangle enclosed by P1, P2, and P3"
  (make-instance 'triangle-constraint
                 :p1 p1
                 :p2 p2
                 :p3 p3))

;;; https://stackoverflow.com/questions/42740765/intersection-between-line-and-triangle-in-3d#42752998
(defmethod violated? ((constraint triangle-constraint)
                      (particle particle))
  (=> (:var p1 (p1<- constraint))
    (:var o (previous-position<- particle))
    (:var dir (v3:- (position<- particle)
                    o))
    (:var e1 (v3:- (p2<- constraint)
                   p1))
    (:var e2 (v3:- (p3<- constraint)
                   p1))
    (:var n (v3:cross e1 e2))
    (:var det (- (v3:dot dir n)))
    (when (>= (abs det)
              1e-6)
      (=> (:var invdet (/ det))
        (:var ao (v3:- o p1))
        (:var dao (v3:cross ao dir))
        (:var u (* (v3:dot e2 dao)
                   invdet))
        (:var v (* (v3:dot e1 dao)
                   (- invdet)))
        (:var x (* (v3:dot ao n)
                   invdet))
        (and (>= 1.0 x 0.0)
             (>= u 0.0)
             (>= v 0.0)
             (<= (+ u v)
                 1.0))))))

(defmethod deflection-vector ((constraint triangle-constraint)
                              (particle particle))
  (bind ((p1 (p1<- particle)))
    (v3:negate (orthogonalize (position<- particle)
                              (p1<- constraint)
                              (v3:cross (v3:- (p2<- constraint)
                                              p1)
                                      (v3:- (p3<- constraint)
                                            p1))))))

(deforce triangle-constraint :constraint ((x1 y1 z1)
                                          (x2 y2 z2)
                                          (x3 y3 z3))
  `(bind ((con ([triangle-constraint] (v! ,x1 ,y1 ,z1)
                                      (v! ,x2 ,y2 ,z2)
                                      (v! ,x3 ,y3 ,z3))))
     (setf (subjects<- con)
           (convert 'set particles))
     con))
