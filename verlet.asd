;;;; verlet.asd

(asdf:defsystem #:verlet
  :description "Verlet is a simple physics engine based on verlet
  integration. It supports particles with position and direction, springs
  between particles, global gravity as well as gravity between particles, and
  spacial constraints."
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.8"
  :depends-on (#:chain
               #:metabang-bind
               #:rtg-math
               #:fset
               #:mgl-pax)
  :components
  ((:file "package")
   (:module "utility"
    :depends-on ("package")
    :components ((:file "algebra")
                 (:file "set")))
   (:module "source"
    :depends-on ("package"
                 "utility")
    :components ((:file "force")
                 (:file "particle")
                 (:file "physics-loop"
                  :depends-on ("force"))
                 (:module "spring"
                  :depends-on ("force" "particle")
                  :components ((:file "linear")
                               (:file "area")
                               (:file "volume")
                               (:file "force"
                                :depends-on ("linear" "area" "volume"))))
                 (:file "modificator"
                  :depends-on ("particle"))
                 (:file "damper"
                  :depends-on ("force" "particle" "modificator"))
                 (:module "gravity"
                  :depends-on ("force" "particle" "modificator")
                  :components ((:file "gravity")
                               (:file "global")
                               (:file "mutual")
                               (:file "field")))
                 (:module "constraint"
                  :depends-on ("force" "particle" "modificator")
                  :components ((:file "constraint")
                               (:file "plane"
                                :depends-on ("constraint"))
                               (:file "sphere"
                                :depends-on ("constraint"))
                               (:file "triangle"
                                :depends-on ("constraint"))))
                 (:module "friction"
                  :depends-on ("force" "particle")
                  :components ((:file "friction")
                               (:file "global"
                                :depends-on ("friction"))
                               (:file "field"
                                :depends-on ("friction"))
                               (:file "constraint"
                                :depends-on ("friction"))))))))
